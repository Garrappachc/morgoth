import { ChildProcess } from "child_process";

import log from "../logger";

/**
 * Describes options with which the server will be started.
 */
export interface IServerLaunchOptions {
    /* Port on which a TF2 server instance will be started. */
    port: number;

    /* Initial map of the server. */
    initialMap: string;

    /* Maximum number of players on the server. */
    maxPlayers: number;

    /* Name of the server config file.
        Default = "server.cfg" */
    serverConfigFile?: string;
}

export interface IServerStatus {
    running: boolean;
    players?: number;
    map?: string;
}

/**
 * An interface that describes a single TF2 server instance.
 */
export interface IServer {
    /* A unique, but human-readable name of the server. */
    name: string;

    /* Where this server instance actually exsits; this directory
        must contain the 'tf' subdirectory. */
    installationPath: string;
}

export class Server implements IServer {
    public id: string;
    public name: string;
    public installationPath: string;
    public status: IServerStatus;

    private _process?: ChildProcess;

    get process(): ChildProcess | undefined {
        return this._process;
    }

    set process(process: ChildProcess | undefined) {
        this._process = process;
        if (process && !process.killed) {
            this.status = { running: true };
        } else {
            this.status = { running: false };
        }
    }

    public stop(): void {
        if (this._process) {
            log.info(this.name + ": stopping");
            this._process.stdin.write("quit");
        }
    }
}
