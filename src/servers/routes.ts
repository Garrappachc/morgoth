import * as express from "express";
import { ServerModel } from "./schema";
import ServerManager from "./server-manager";
import { routes as tasks } from "./tasks";

import log from "../logger";

const routes = express.Router();

routes.route("/")
    .get((req, res) => {
        res.json(ServerManager.allServers());
    })
    .post((req, res) => {
        const server = new ServerModel();
        server.name = req.body.name;
        server.installationPath = req.body.installationPath;

        server.save()
            .then(() => {
                res
                    .status(201)
                    .location("./" + String(server._id))
                    .json({ message: server.name + " created"});
            })
            .catch((error) => res.send(error));
    });

routes.route("/:server_id")
    .get((req, res) => {
        ServerModel.findById(req.params.server_id)
            .then((server) => res.json(server))
            .catch((error) => res.send(error));
    })
    .delete((req, res) => {
        ServerModel.remove({ _id: req.params.server_id })
            .then(() => res.json({ message: "Server deleted"} ))
            .catch((error) => res.send(error));
    });

routes.use("/:server_id/task", tasks);

export default routes;
