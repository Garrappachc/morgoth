import { IServerModel, ServerModel } from "./schema";
import { Server } from "./server";

import log from "../logger";

class ServerManager {
    private readonly servers: Map<string, Server> = new Map();

    constructor() {
        // init all servers from the database
        ServerModel.find()
            .then((servers) => {
                for (const server of servers) {
                    this.initializeServer(server);
                }
            })
            .catch((error) => log.error(error));
    }

    public allServers(): Server[] {
        return Array.from(this.servers.values());
    }

    public getById(id: string): Server | undefined {
        return this.servers.get(id);
    }

    private initializeServer(model: IServerModel): void {
        const server: Server = new Server();
        server.id = String(model._id);
        server.name = model.name;
        server.installationPath = model.installationPath;

        server.status = { running: false };

        this.servers.set(server.id, server);
        log.info(server.name + ": initialized");
    }
}

export default new ServerManager();
