import * as express from "express";
import log from "../../logger";
import startServer from "./start-server";

import { ServerManager } from "..";

const routes = express.Router({ mergeParams: true });

routes.route("/start")
    .post((req, res) => {
        const server = ServerManager.getById(req.params.server_id);
        if (!server) {
            res.status(404).json({ message: "Server not found" });
            return;
        }

        startServer(server);
        res.status(202).json({ message: "Task ordered"});
    });

routes.route("/stop")
    .post((req, res) => {
        const server = ServerManager.getById(req.params.server_id);
        if (!server) {
            res.status(404).json({ message: "Server not found" });
            return;
        }

        server.stop();
        res.status(202).json({ message: "Task ordered" });
    });

export default routes;
