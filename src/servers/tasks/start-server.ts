import { spawn } from "child_process";
import { join } from "path";

import { IServerLaunchOptions, Server } from "..";
import log from "../../logger";

const SrcdsExec = "srcds_linux";

export default function startServer(server: Server): void {
    const launchOptions: IServerLaunchOptions = {
        initialMap: "cp_badlands",
        maxPlayers: 24,
        port: 27015,
    };

    const srcds = join(server.installationPath, SrcdsExec);
    const libraryPath = [
        join(server.installationPath, "bin"),
        process.env.LD_LIBRARY_PATH,
    ].join(":");

    server.process = spawn(srcds, [
        "-game", "tf",
        "-port", String(launchOptions.port),
        "+map", launchOptions.initialMap,
        "+maxplayers", String(launchOptions.maxPlayers),
        "-secured",
        "-timeout", "0",
        "+servercfgfile", launchOptions.serverConfigFile || "server.cfg",
    ], {
        env: Object.assign({}, process.env, { LD_LIBRARY_PATH: libraryPath }),
        stdio: [ 0, 1, 2 ],
    });

    log.info(server.name + ": starting");

    // server.process.stdout.on("data", (data) => {
    //     log.verbose(data instanceof Buffer ? data.toString() : data);
    // });
    // server.process.stderr.on("data", (data) => {
    //     log.error(data instanceof Buffer ? data.toString() : data);
    // });
    // server.process.stdout.on("close", () => log.info(server.name + " stopped"));
}
