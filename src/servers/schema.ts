import { Document, Model, model, Schema } from "mongoose";
import { IServer, IServerLaunchOptions } from "./server";

const ServerSchema: Schema = new Schema({
    installationPath: String,
    name: String,
});
export interface IServerModel extends IServer, Document {}
export const ServerModel: Model<IServerModel> = model<IServerModel>("Server", ServerSchema);

const ServerLaunchOptionsSchema: Schema = new Schema({
    initialMap: String,
    maxPlayers: Number,
    port: Number,
    serverConfigFile: String,
    serverId: Schema.Types.ObjectId,
});
interface IServerLaunchOptionsModel extends IServerLaunchOptions, Document {}
export const ServerLaunchOptionsModel: Model<IServerLaunchOptionsModel> =
    model<IServerLaunchOptionsModel>("ServerLaunchOptionsModel", ServerLaunchOptionsSchema);
