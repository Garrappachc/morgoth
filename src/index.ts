import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as mongoose from "mongoose";

import { initialize as authenticate, protect } from "./authentication";
import log from "./logger";
import { routes as servers } from "./servers";

const port = process.env.PORT || 3000;
const app = express();

mongoose.connect("mongodb://localhost:27017/morgoth");

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

authenticate(app);
app.use("/servers", protect, servers);

app.listen(port);
log.verbose("morgoth API server started on: " + port);
