interface ISteamConfiguration {
    apiKey: string;
}

interface IServer {
    name: string;
}

interface IConfiguration {
    url: string;
    steam: ISteamConfiguration;
    servers: IServer[];
}

// tslint:disable-next-line:no-var-requires
const config: IConfiguration = require("../../config/default.json");

export default config;
