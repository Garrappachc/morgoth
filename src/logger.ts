import * as winston from "winston";

const log = winston;

log.configure({
    level: "verbose",
    transports: [new winston.transports.Console()],
});

export default log;
