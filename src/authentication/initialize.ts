import * as express from "express";
import * as session from "express-session";
import * as passport from "passport";
import steam = require("passport-steam");
import config from "../config";

function route(app: express.Application): void {
    app.get("/auth/steam", passport.authenticate("steam"));
    app.get("/auth/steam/return",
        passport.authenticate("steam", { failureRedirect: "/" }),
        (req, res) => res.redirect("/servers"));
}

export default function initialize(app: express.Application): void {
    passport.serializeUser((user, done) => {
        done(null, user);
    });

    passport.deserializeUser((object, done) => {
        done(null, object);
    });

    passport.use(new steam.Strategy({
        apiKey: config.steam.apiKey,
        realm: config.url,
        returnURL: config.url + "/auth/steam/return",
    }, (identifier: any, profile: any, done: any) => {
        profile.identifier = identifier;
        return done(null, profile);
    }));

    app.use(session({
        resave: true,
        saveUninitialized: true,
        secret: "dupa13",
    }));

    app.use(passport.initialize());
    app.use(passport.session());

    route(app);
}
